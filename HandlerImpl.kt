import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit
import kotlin.time.Duration
import kotlin.time.DurationUnit
import kotlin.time.toDuration

class HandlerImpl : Handler {
    private val workerPool: ExecutorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors())
    private val workerScheduledPool: ScheduledExecutorService = Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors())
    override val timeout: Duration
        get() = 1000L.toDuration(DurationUnit.MILLISECONDS)

    override fun performOperation() {
        val client = ClientImpl()                   // some implementation of client
        val event = client.readData()
        event.recipients.forEach {
            workerPool.submit {
                val res = client.sendData(it, event.payload)
                if (res == Result.REJECTED) {
                    retryTask(client, it, event.payload)
                }
            }
        }
    }

    private fun retryTask(client: Client, recipient: Address, payload: Payload): Runnable = Runnable {
        workerScheduledPool.scheduleAtFixedRate(
                {
                    val res = client.sendData(recipient, payload)
                    if (res == Result.REJECTED) {
                        retryTask(client, recipient, payload)
                    }
                },
                timeout.toLong(DurationUnit.MILLISECONDS),
                0,
                TimeUnit.MILLISECONDS
        )
    }
}